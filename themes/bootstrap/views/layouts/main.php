<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
    
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
	'type'=>'inverse',
	'brand'=>'Absensi YPKP',
	'brandUrl'=>'#',
	'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'Jadwal', 'url'=>'#', 'items'=>array(
                    array('label'=>'Reguler', 'url'=>array('/jadwal/listjadwal')),
                    array('label'=>'Pergantian', 'url'=>array('/pergantian/listpergantian')),
		)),
                
		array('label'=>'Kelola Data', 'url'=>'#', 'items'=>array(
                    array('label'=>'Karyawan', 'url'=>array('/karyawan')),
                    array('label'=>'Jabatan', 'url'=>array('/jenis')),
                    array('label'=>'Kelas', 'url'=>array('/kelas')),
                    array('label'=>'Ruangan', 'url'=>array('/ruangan')),
                    array('label'=>'Mata Kuliah', 'url'=>array('/matakuliah')),
                    array('label'=>'Jadwal', 'url'=>array('/jadwal')),
                    array('label'=>'User', 'url'=>array('/user')),
		),'visible'=>Yii::app()->user->getLevel()<=1),
                array('label'=>'Daftar Absensi', 'url'=>array('/absensi'),'visible'=>Yii::app()->user->getLevel()<=1 ),
                array('label'=>'Absensi', 'url'=>array('/absensi/riwayatabsen'),'visible'=>Yii::app()->user->getLevel()>1 and Yii::app()->user->getLevel()<=10 ),
		array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
		array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
    	<br></br><br></br>
		Copyright &copy; <?php echo date('Y'); ?> by Kelompok 4 3IF<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
