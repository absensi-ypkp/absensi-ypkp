<?php

class AbsensiController extends Controller
{
       
      
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
                        array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('updatepergantian','deletepergantian','createpergantian','riwayatabsen','viewriwayat','create','viewpergantian'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('admin','index','delete','update','view'),
				'expression'=>'$user->getLevel()<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $absensi = $this->loadModel($id);
		$criteria = new CDbCriteria();
		$criteria->addCondition('id_absensi = ' . $id);
		$data = new CActiveDataProvider('Pergantian', array(
			'criteria' => $criteria
		));

		$this->render('view', array(
			'model' => $absensi,
			'data' => $data,
                      
		));
                
                 
	}
        
        public function actionViewriwayat($id)
	{
                $absensi = $this->loadModel($id);
		$criteria = new CDbCriteria();
		$criteria->addCondition('id_absensi = ' . $id);
		$data = new CActiveDataProvider('Pergantian', array(
			'criteria' => $criteria
		));

		$this->render('viewriwayat', array(
			'model' => $absensi,
			'data' => $data,
                      
		));
                
                 
	}
        
         public function actionViewpergantian($id)
	{
                $absensi = $this->loadModel($id);
		$criteria = new CDbCriteria();
                $nip=Yii::app()->user->getnip();
		$criteria->condition = "id_karyawan = :id_karyawan AND hari = :hari";
                $criteria->params = array (	
                ':id_karyawan' => $nip,
                ':hari' => $absensi->hari,
                );
		$data = new CActiveDataProvider('Jadwal', array(
			'criteria' => $criteria
		));

		$this->render('viewpergantian', array(
			'model' => $absensi,
			'data' => $data,
                      
		));
                
                 
	}
        
        public function actionCreatepergantian($id)
	{
		$model=new Pergantian();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $criteria=new CDbCriteria;      //kita menggunakan criteria untuk mengetahui nomor terakhir dari database
                $criteria->select = 'id';   //yang ingin kita lihat adalah field "nilai1"
                $criteria->limit=1;             // kita hanya mengambil 1 buah nilai terakhir
                $criteria->order='id DESC';  //yang dimbil nilai terakhir
                $last = Absensi::model()->find($criteria);
		if(isset($_POST['Pergantian']))
		{
			$model->attributes=$_POST['Pergantian'];
                        $model->id_absensi=$last->id;
                        $model->kode_jadwal=$id;
			if($model->save())
				$this->redirect(array('viewpergantian','id'=>$model->id_absensi));
		}

		$this->render('createpergantian',array(
			'model'=>$model,
		));
	}
        
         public function actionUpdatepergantian($id)
	{
		$model = $this->loadpergantian($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['Pergantian']))
		{
			$model->attributes=$_POST['Pergantian'];
			if($model->save())
				$this->redirect(array('viewriwayat','id'=>$model->id_absensi));
		}

		$this->render('updatepergantian',array(
			'model'=>$model,
		));
	}
        
       

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Absensi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Absensi']))
		{
			$model->attributes=$_POST['Absensi'];
                        $model->id_karyawan=Yii::app()->user->getnip();
			if($model->save())
				$this->redirect(array('viewpergantian','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
                
                
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Absensi']))
		{
			$model->attributes=$_POST['Absensi'];
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
                
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
        public function actionIndex()
	{
		
                
                $model=new Absensi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Absensi']))
			$model->attributes=$_GET['Absensi'];

		$this->render('index',array(
			'model'=>$model,
		));
                
	}

	/**
	 * Manages all models.
	 */
	public function actionRiwayatabsen()
	{
          
                $criteria = new CDbCriteria();
                $id=Yii::app()->user->getnip();
		$criteria->addCondition('id_karyawan = ' . $id);
		$data = new CActiveDataProvider('Absensi', array(
			'criteria' => $criteria
		));

		$this->render('riwayatabsen',array(
			
                        'data' => $data,
		));    
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Absensi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Absensi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        private function loadpergantian($id)
	{
		$model = Pergantian::model()->findByPk($id);
		if($model===null) {
			throw new CHttpException(404, 'Buku tidak ditemukan');
		}
		return $model;
	}
        
        public function actionDeletepergantian($id)
	{
		$this->loadpergantian($id)->delete();
                
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('view'));
	}

	/**
	 * Performs the AJAX validation.
	 * @param Absensi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='absensi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
