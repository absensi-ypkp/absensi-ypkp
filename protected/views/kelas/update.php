<?php
/* @var $this KelasController */
/* @var $model Kelas */

$this->breadcrumbs=array(
	'Kelas'=>array('index'),
	$model->nama_kelas=>array('view','id'=>$model->kode_kelas),
	'Ubah',
);

$this->menu=array(
	array('label'=>'List Kelas', 'url'=>array('index')),
	array('label'=>'Create Kelas', 'url'=>array('create')),
	array('label'=>'View Kelas', 'url'=>array('view', 'id'=>$model->kode_kelas)),
	array('label'=>'Manage Kelas', 'url'=>array('admin')),
);
?>

<h1>Ubah Data Kelas #<?php echo $model->nama_kelas; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>