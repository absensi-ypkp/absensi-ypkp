<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
        //'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div class="row">
		<?php echo $form->textFieldRow($model,'id_karyawan', array('size'=>20,'maxlength'=>20)); ?>
		
	</div>

	<div class="row">
                <?php echo $form->textFieldRow($model,'username',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->passwordFieldRow($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->passwordFieldRow($model,'password2',array('size'=>50,'maxlength'=>50)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		
	</div>
        
        <div class="row">
		<?php echo $form->dropDownListRow($model,'level_id', CHtml::listData(
                 Level::model()->findAll(), 'id', 'level'),
			array(
				'empty' => '=== Pilih Level =='
			)
		); ?><?php echo $form->error($model,'level_id'); ?>
		
	</div>
        
        <div class="row">
            <div class="cotrol-group">
                <?php if (extension_loaded('gd')): ?>
                    <div class="control-label required" > 
                        <?php echo CHtml::activeLabelEx($model, 'verifyCode')?> 
                    </div>
                
                    <div class="controls" > 
                        <?php $this->widget('CCaptcha'); ?><br/> 
                        <?php echo CHtml::activeTextField($model,'verifyCode'); ?> 
                    </div> 
                
                    <div class="controls" > 
                        <div class="hint">Ketik tulisan yang ada pada gambar . 
                        <br/>Tulisan tidak case sensitive</div> 
                    </div>
                
        <?php endif; ?>
            </div>
        </div>
	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Simpan',
        )); ?>
        
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'reset',
            'label'=>'Batal',
            'url'=>array('index'),
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->