<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>
        
        <div class="row">
		<?php echo $form->textFieldRow($model,'id_karyawan',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'username',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
            <div class="cotrol-group">
                <div class="control-label required" >
                    <?php echo $form->labelEx($model,'joinDate'); ?>
                </div>
                <div class="controls" > 
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                    array(
                        'model'=>$model,
                        'attribute'=>'joinDate',
                        'options' => array(
                            
                            'dateFormat'=>'yy-mm-dd',
                        ),
                    ));?>
                </div>
            </div>
	</div>
    </br>
	<div class="row">
		
                <?php echo $form->dropDownListRow($model,'level_id', CHtml::listData(
			Level::model()->findAll(), 'id', 'level'),
			array(
				'empty' => '=== Pilih Level =='
			)
		); ?>
	</div>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Cari',
        )); ?>
	</div>


<?php $this->endWidget(); ?>

</div><!-- search-form -->