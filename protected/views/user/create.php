<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Pengguna'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Pengguna</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>