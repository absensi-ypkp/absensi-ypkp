<?php
/* @var $this PergantianController */
/* @var $data Pergantian */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pergantian')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pergantian), array('view', 'id'=>$data->id_pergantian)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_absensi')); ?>:</b>
	<?php echo CHtml::encode($data->id_absensi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_jadwal')); ?>:</b>
	<?php echo CHtml::encode($data->kode_jadwal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ruangan')); ?>:</b>
	<?php echo CHtml::encode($data->id_ruangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hari')); ?>:</b>
	<?php echo CHtml::encode($data->hari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jammulai')); ?>:</b>
	<?php echo CHtml::encode($data->jammulai); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jamselesai')); ?>:</b>
	<?php echo CHtml::encode($data->jamselesai); ?>
	<br />

	*/ ?>

</div>