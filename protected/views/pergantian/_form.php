<?php
/* @var $this PergantianController */
/* @var $model Pergantian */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pergantian-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_absensi'); ?>
		<?php echo $form->textField($model,'id_absensi'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kode_jadwal'); ?>
		<?php echo $form->textField($model,'kode_jadwal'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_ruangan'); ?>
		<?php echo $form->textField($model,'id_ruangan'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->textField($model,'tanggal'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hari'); ?>
		<?php echo $form->textField($model,'hari',array('size'=>10,'maxlength'=>10)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jammulai'); ?>
		<?php echo $form->textField($model,'jammulai',array('size'=>8,'maxlength'=>8)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jamselesai'); ?>
		<?php echo $form->textField($model,'jamselesai',array('size'=>8,'maxlength'=>8)); ?>
		
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->