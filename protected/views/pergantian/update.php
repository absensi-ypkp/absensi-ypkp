<?php
/* @var $this PergantianController */
/* @var $model Pergantian */

$this->breadcrumbs=array(
	'Pergantians'=>array('index'),
	$model->id_pergantian=>array('view','id'=>$model->id_pergantian),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pergantian', 'url'=>array('index')),
	array('label'=>'Create Pergantian', 'url'=>array('create')),
	array('label'=>'View Pergantian', 'url'=>array('view', 'id'=>$model->id_pergantian)),
	array('label'=>'Manage Pergantian', 'url'=>array('admin')),
);
?>

<h1>Update Pergantian <?php echo $model->id_pergantian; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>