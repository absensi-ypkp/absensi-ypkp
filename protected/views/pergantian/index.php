<?php
/* @var $this PergantianController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pergantians',
);

$this->menu=array(
	array('label'=>'Create Pergantian', 'url'=>array('create')),
	array('label'=>'Manage Pergantian', 'url'=>array('admin')),
);
?>

<h1>Pergantians</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
