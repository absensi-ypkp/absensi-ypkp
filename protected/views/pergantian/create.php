<?php
/* @var $this PergantianController */
/* @var $model Pergantian */

$this->breadcrumbs=array(
	'Pergantians'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pergantian', 'url'=>array('index')),
	array('label'=>'Manage Pergantian', 'url'=>array('admin')),
);
?>

<h1>Create Pergantian</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>