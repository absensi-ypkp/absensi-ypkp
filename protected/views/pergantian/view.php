<?php
/* @var $this PergantianController */
/* @var $model Pergantian */

$this->breadcrumbs=array(
	'Pergantians'=>array('index'),
	$model->id_pergantian,
);

$this->menu=array(
	array('label'=>'List Pergantian', 'url'=>array('index')),
	array('label'=>'Create Pergantian', 'url'=>array('create')),
	array('label'=>'Update Pergantian', 'url'=>array('update', 'id'=>$model->id_pergantian)),
	array('label'=>'Delete Pergantian', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pergantian),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pergantian', 'url'=>array('admin')),
);
?>

<h1>View Pergantian #<?php echo $model->id_pergantian; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pergantian',
		'id_absensi',
		'kode_jadwal',
		'id_ruangan',
		'tanggal',
		'hari',
		'jammulai',
		'jamselesai',
	),
)); ?>
