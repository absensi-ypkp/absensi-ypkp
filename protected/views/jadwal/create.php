<?php
/* @var $this JadwalController */
/* @var $model Jadwal */

$this->breadcrumbs=array(
	'Jadwal'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Jadwal', 'url'=>array('index')),
	array('label'=>'Manage Jadwal', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Jadwal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>