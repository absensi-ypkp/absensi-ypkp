<?php
/* @var $this JadwalController */
/* @var $model Jadwal */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'jadwal-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
                
		<?php echo $form->dropDownListRow($model,'id_karyawan', CHtml::listData(
                    Karyawan::model()->findAll('jenis_id = 2'), 'nip', 'nama'),
			array(
				'empty' => '=== Pilih Dosen =='
			)
		); ?>
		
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_mtk', CHtml::listData(
                        Matakuliah::model()->findAll(), 'id', 'nama'),
			array(
				'empty' => '=== Pilih Mata Kuliah =='
			)
		); ?>
		
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'kode_kelas', CHtml::listData(
                    Kelas::model()->findAll(), 'kode_kelas', 'nama_kelas'),
			array(
				'empty' => '=== Pilih Kelas =='
			)
		); ?>
		
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_ruangan', CHtml::listData(
                    Ruangan::model()->findAll(), 'id', 'kode'),
			array(
				'empty' => '=== Pilih Ruangan =='
			)
		); ?>
		
	</div>
        
        <div class="row">
		<?php echo $form->dropDownListRow($model,'hari',
			Jadwal::hari(),
			array('prompt'=>'===  Pilih Hari  ===',));
		?>
                
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'jammulai',array('size'=>8,'maxlength'=>8)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'jamselesai',array('size'=>8,'maxlength'=>8)); ?>
		
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Simpan',
        )); ?>
        
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'reset',
            'label'=>'Batal',
            'url'=>array('index'),
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->