<?php
/* @var $this JadwalController */
/* @var $model Jadwal */

$this->breadcrumbs=array(
	'Jadwal'=>array('index'),
	$model->kodeKelas->nama_kelas=>array('view','id'=>$model->kode_jadwal),
	'Ubah',
);

$this->menu=array(
	array('label'=>'List Jadwal', 'url'=>array('index')),
	array('label'=>'Create Jadwal', 'url'=>array('create')),
	array('label'=>'View Jadwal', 'url'=>array('view', 'id'=>$model->kode_jadwal)),
	array('label'=>'Manage Jadwal', 'url'=>array('admin')),
);
?>

<h1>Ubah Data Jadwal #<?php echo $model->kodeKelas->nama_kelas; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>