<?php
/* @var $this JadwalController */
/* @var $model Jadwal */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_karyawan', CHtml::listData(
                    Karyawan::model()->findAll(), 'nip', 'nama'),
			array(
				'empty' => '=== Pilih Dosen =='
			)
		); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_mtk', CHtml::listData(
                        Matakuliah::model()->findAll(), 'id', 'nama'),
			array(
				'empty' => '=== Pilih Mata Kuliah =='
			)
		); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'kode_kelas', CHtml::listData(
                    Kelas::model()->findAll(), 'kode_kelas', 'nama_kelas'),
			array(
				'empty' => '=== Pilih Kelas =='
			)
		); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_ruangan', CHtml::listData(
                    Ruangan::model()->findAll(), 'id', 'kode'),
			array(
				'empty' => '=== Pilih Ruangan =='
			)
		); ?>
	</div>
    
        <div class="row">
		<?php echo $form->dropDownListRow($model,'hari',
			Jadwal::hari(),
			array('prompt'=>'===  Pilih Hari  ===',));
		?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'jammulai',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'jamselesai',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Cari',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->