<?php
/* @var $this JadwalController */
/* @var $model Jadwal */

$this->breadcrumbs=array(
	'Jadwal'=>array('index'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'List Jadwal', 'url'=>array('index')),
	array('label'=>'Create Jadwal', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jadwal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Data Jadwal</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'jadwal-grid',
	'dataProvider'=>$model->search(),
        'template'=>"{items}",
	'columns'=>array(
                 array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
                array(
                    'name'=> 'id_mtk',
                    'value'=>function($data){
                        return $data->idMtk->nama;
                    }
		),
                 array(
                    'name'=> 'id_ruangan',
                    'value'=>function($data){
                        return $data->idRuangan->kode;
                    }
		),
                 array(
                    'name'=> 'kode_kelas',
                    'value'=>function($data){
                        return $data->kodeKelas->nama_kelas;
                    }
		),
		'hari',
                 array(
                    'name'=> 'jam',
                    'value'=>function($data){
                        return $data->jammulai." s/d ".$data->jamselesai;
                    }
		),
		 array(
                    'name'=> 'id_karyawan',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
		
		
		
		/*
		'jammulai',
		'jamselesai',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
		),
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah Data',
			'url'=>array('create'),
		)); ?>
   </td>
  </tr>
</table>
