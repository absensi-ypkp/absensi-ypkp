<?php
/* @var $this JadwalController */
/* @var $model Jadwal */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jadwal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Jadwal Mata Kuliah</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'jadwal-grid',
	'dataProvider'=>$model->search(),
        'template'=>"{items}",
	'columns'=>array(
                array(
                    'name'=> 'Mata Kuliah',
                    'value'=>function($data){
                        return $data->idMtk->nama;
                    }
		),
                 array(
                    'name'=> 'Ruangan',
                    'value'=>function($data){
                        return $data->idRuangan->kode;
                    }
		),
                 array(
                    'name'=> 'Kelas',
                    'value'=>function($data){
                        return $data->kodeKelas->nama_kelas;
                    }
		),
		array(
                    'name'=> 'Hari',
                    'value'=>function($data){
                        return $data->hari;
                    }
		),
                 array(
                    'name'=> 'Jam',
                    'value'=>function($data){
                        return $data->jammulai." s/d ".$data->jamselesai;
                    }
		),
		 array(
                    'name'=> 'Dosen',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
		
		
		
		/*
		'jammulai',
		'jamselesai',
		*/
	),
)); ?>
