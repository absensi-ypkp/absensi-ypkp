<?php
/* @var $this JadwalController */
/* @var $model Jadwal */

$this->breadcrumbs=array(
	'Jadwal'=>array('index'),
	$model->kodeKelas->nama_kelas,
);

$this->menu=array(
	array('label'=>'List Jadwal', 'url'=>array('index')),
	array('label'=>'Create Jadwal', 'url'=>array('create')),
	array('label'=>'Update Jadwal', 'url'=>array('update', 'id'=>$model->kode_jadwal)),
	array('label'=>'Delete Jadwal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kode_jadwal),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Jadwal', 'url'=>array('admin')),
);
?>

<h1>Lihat Data Jadwal #<?php echo $model->kodeKelas->nama_kelas; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=> 'id_mtk',
                    'value'=>function($data){
                        return $data->idMtk->nama;
                    }
		),
		 array(
                    'name'=> 'id_ruangan',
                    'value'=>function($data){
                        return $data->idRuangan->kode;
                    }
		),
		array(
                    'name'=> 'kode_kelas',
                    'value'=>function($data){
                        return $data->kodeKelas->nama_kelas;
                    }
		),
                'hari',
		'jammulai',
		'jamselesai',
                 array(
                    'name'=> 'id_karyawan',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Kembali',
			'url'=>array('index'),
		)); ?>
   </td>
  </tr>
</table>
