<?php
/* @var $this MatakuliahController */
/* @var $model Matakuliah */

$this->breadcrumbs=array(
	'Mata Kuliah'=>array('index'),
	$model->nama=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'List Matakuliah', 'url'=>array('index')),
	array('label'=>'Create Matakuliah', 'url'=>array('create')),
	array('label'=>'View Matakuliah', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Matakuliah', 'url'=>array('admin')),
);
?>

<h1>Ubah Data Mata Kuliah #<?php echo $model->nama; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>