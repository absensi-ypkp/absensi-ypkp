<?php
/* @var $this MatakuliahController */
/* @var $model Matakuliah */

$this->breadcrumbs=array(
	'Mata Kuliah'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Matakuliah', 'url'=>array('index')),
	array('label'=>'Manage Matakuliah', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Mata Kuliah</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>