<?php
/* @var $this MatakuliahController */
/* @var $model Matakuliah */

$this->breadcrumbs=array(
	'Mata Kuliah'=>array('index'),
	$model->nama,
);

$this->menu=array(
	array('label'=>'List Matakuliah', 'url'=>array('index')),
	array('label'=>'Create Matakuliah', 'url'=>array('create')),
	array('label'=>'Update Matakuliah', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Matakuliah', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Matakuliah', 'url'=>array('admin')),
);
?>

<h1>Lihat Data Mata Kuliah #<?php echo $model->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'nama',
		'sks',
		'semester',
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Kembali',
			'url'=>array('index'),
		)); ?>
   </td>
  </tr>
</table>
