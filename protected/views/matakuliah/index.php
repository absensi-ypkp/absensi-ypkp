<?php
/* @var $this MatakuliahController */
/* @var $model Matakuliah */

$this->breadcrumbs=array(
	'Mata Kuliah'=>array('index'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'List Matakuliah', 'url'=>array('index')),
	array('label'=>'Create Matakuliah', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#matakuliah-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Data Mata Kuliah</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'matakuliah-grid',
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{items}",
	'columns'=>array(
                 array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
		'kode',
		'nama',
                array(
		'name'=> 'sks',
                        'filter'=>CHtml::activeDropDownList($model, 'sks',
				Matakuliah::sks(),
				array('prompt'=>'--  SKS  --')
			),
			'value'=>function($data){
				return $data->sks;
			}
		),

                array(
		'name'=> 'semester',
                        'filter'=>CHtml::activeDropDownList($model, 'semester',
				Matakuliah::semester(),
				array('prompt'=>'--  Semester  --')
			),
			'value'=>function($data){
				return $data->semester;
			}
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
		),
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah Data',
			'url'=>array('create'),
		)); ?>
   </td>
  </tr>
</table>
