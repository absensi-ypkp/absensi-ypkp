<?php
/* @var $this MatakuliahController */
/* @var $model Matakuliah */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<div class="row">
		<?php echo $form->textFieldRow($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'kode',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'nama',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'sks',
			Matakuliah::sks(),
			array('prompt'=>'--  SKS  --',));
		?>
	</div>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'semester',
			Matakuliah::semester(),
			array('prompt'=>'--  Semester  --',));
		?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>'Cari',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->