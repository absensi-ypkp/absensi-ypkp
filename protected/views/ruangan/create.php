<?php
/* @var $this RuanganController */
/* @var $model Ruangan */

$this->breadcrumbs=array(
	'Ruangan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Ruangan', 'url'=>array('index')),
	array('label'=>'Manage Ruangan', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Ruangan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>