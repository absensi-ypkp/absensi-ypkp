<?php
/* @var $this RuanganController */
/* @var $model Ruangan */

$this->breadcrumbs=array(
	'Ruangan'=>array('index'),
	$model->nama,
);

$this->menu=array(
	array('label'=>'List Ruangan', 'url'=>array('index')),
	array('label'=>'Create Ruangan', 'url'=>array('create')),
	array('label'=>'Update Ruangan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ruangan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ruangan', 'url'=>array('admin')),
);
?>

<h1>Lihat Data Ruangan #<?php echo $model->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'nama',
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Kembali',
			'url'=>array('index'),
		)); ?>
   </td>
  </tr>
</table>
