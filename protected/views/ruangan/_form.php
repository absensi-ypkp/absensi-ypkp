<?php
/* @var $this RuanganController */
/* @var $model Ruangan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ruangan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->textFieldRow($model,'kode',array('size'=>3,'maxlength'=>3)); ?>
		
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'nama',array('size'=>50,'maxlength'=>50)); ?>
		
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Simpan',
        )); ?>
        
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'reset',
            'label'=>'Batal',
            'url'=>array('index'),
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->