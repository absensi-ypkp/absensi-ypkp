<?php
/* @var $this RuanganController */
/* @var $model Ruangan */

$this->breadcrumbs=array(
	'Ruangan'=>array('index'),
	$model->nama=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'List Ruangan', 'url'=>array('index')),
	array('label'=>'Create Ruangan', 'url'=>array('create')),
	array('label'=>'View Ruangan', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Ruangan', 'url'=>array('admin')),
);
?>

<h1>Ubah Data Ruangan #<?php echo $model->nama; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>