<?php
/* @var $this AbsensiController */
/* @var $model Absensi */
?>

<h1>Lihat Data Absensi #<?php echo $model->idKaryawan->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=> 'Nip',
                    'value'=>function($data){
                        return $data->id_karyawan;
                    }
		),
                array(
                    'name'=> 'Nama',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
		'tanggal',
		'hari',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'id'=>'grid_pergantian',
	'dataProvider'=>$data,
        'template'=>"{items}",
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
                array(
                    'name'=> 'Tanggal',
                    'value'=>function($data){
                        return $data->tanggal;
                    }
		),
       
		 array(
                    'name'=> 'Mata Kuliah',
                    'value'=>function($data){
                        return $data->kodeJadwal->idMtk->nama;
                    }
		),
                 array(
                    'name'=> 'Ruangan',
                    'value'=>function($data){
                        return $data->kodeJadwal->idRuangan->kode;
                    }
		),
                array(
                    'name'=> 'Kelas',
                    'value'=>function($data){
                        return $data->kodeJadwal->kodeKelas->nama_kelas;
                    }
		),
                array(
                    'name'=> 'Hari',
                    'value'=>function($data){
                        return $data->hari;
                    }
		),
                 array(
                    'name'=> 'jam',
                    'value'=>function($data){
                        return $data->jammulai." s/d ".$data->jamselesai;
                    }
		),
                 array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
               
			'template' => '{update} {delete}',
			'buttons' => array(
                                'update' => array(
					'url' => 'Yii::app()->createUrl("/absensi/updatepergantian", array("id" => $data->id_pergantian))'
				),
                                'delete' => array(
					'url' => 'Yii::app()->createUrl("/absensi/deletepergantian", array("id" => $data->id_pergantian))'
				)
			)
		),
	),
));?>
                
<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah',
			'url'=>array('viewpergantian','id'=>$model->id),
		)); ?>
   </td>
  </tr>
</table>
