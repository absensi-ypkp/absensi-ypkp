<?php
/* @var $this AbsensiController */
/* @var $model Absensi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'id_karyawan', CHtml::listData(
                    Karyawan::model()->findAll('jenis_id=2'), 'nip', 'nama'),
			array(
				'empty' => '=== Pilih Ruangan =='
			)
		); ?>
	</div>

	<div class="row">
            <div class="cotrol-group">
                <div class="control-label required" >
                    <?php echo $form->labelEx($model,'tanggal'); ?>
                </div>
                <div class="controls" > 
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                    array(
                        'model'=>$model,
                        
                        'attribute'=>'tanggal',
                        'options' => array(
                            
                            'dateFormat'=>'yy-mm-dd',
                        ),
                    ));?>
                </div>
            </div>
	</div>
    </br>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'hari',
			Jadwal::hari(),
			array('prompt'=>'===  Pilih Hari  ===',));
		?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Cari',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->