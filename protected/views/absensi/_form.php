<?php
/* @var $this AbsensiController */
/* @var $model Absensi */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'absensi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <div class="cotrol-group">
                <div class="control-label required" >
                    <?php echo $form->labelEx($model,'tanggal'); ?>
                </div>
                <div class="controls" > 
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                    array(
                        'model'=>$model,
                        
                        'attribute'=>'tanggal',
                        'options' => array(
                            
                            'dateFormat'=>'yy-mm-dd',
                        ),
                    ));?>
                </div>
            </div>
	</div>
    </br>

	<div class="row">
		<?php echo $form->dropDownListRow($model,'hari',
			Jadwal::hari(),
			array('prompt'=>'===  Pilih Hari  ===',));
		?>
		
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Simpan',
        )); ?>
        
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'reset',
            'label'=>'Batal',
            'url'=>array('index'),
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->