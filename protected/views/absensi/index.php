<?php
/* @var $this AbsensiController */
/* @var $model Absensi */

$this->breadcrumbs=array(
	'Absensi'=>array('index'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'List Absensi', 'url'=>array('index')),
	array('label'=>'Create Absensi', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#absensi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Absensi</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'absensi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{items}",
	'columns'=>array(
		'id_karyawan',
		'tanggal',
		'hari',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
              
			'template' => '{view} {delete}',
			'buttons' => array(
				'view' => array(
					'url' => 'Yii::app()->createUrl("/absensi/view", array("id" => $data->id))'
				),
                                'delete' => array(
					'url' => 'Yii::app()->createUrl("/absensi/delete", array("id" => $data->id))'
				)
			)
		),
		
	),
)); ?>
