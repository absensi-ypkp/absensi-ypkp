<?php
/* @var $this AbsensiController */
/* @var $model Absensi */

$this->breadcrumbs=array(
	'Absensi'=>array('index'),
	$model->idKaryawan->nama,
);

$this->menu=array(
	array('label'=>'List Absensi', 'url'=>array('index')),
	array('label'=>'Create Absensi', 'url'=>array('create')),
	array('label'=>'Update Absensi', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Absensi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Absensi', 'url'=>array('admin')),
);
?>

<h1>Lihat Data Absensi #<?php echo $model->idKaryawan->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=> 'Nip',
                    'value'=>function($data){
                        return $data->id_karyawan;
                    }
		),
                array(
                    'name'=> 'Nama',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
		'tanggal',
		'hari',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'id'=>'grid_pergantian',
	'dataProvider'=>$data,
        'template'=>"{items}",
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
                array(
                    'name'=> 'Tanggal',
                    'value'=>function($data){
                        return $data->tanggal;
                    }
		),
       
		 array(
                    'name'=> 'Mata Kuliah',
                    'value'=>function($data){
                        return $data->kodeJadwal->idMtk->nama;
                    }
		),
                 array(
                    'name'=> 'Ruangan',
                    'value'=>function($data){
                        return $data->kodeJadwal->idRuangan->kode;
                    }
		),
                array(
                    'name'=> 'Kelas',
                    'value'=>function($data){
                        return $data->kodeJadwal->kodeKelas->nama_kelas;
                    }
		),
                array(
                    'name'=> 'Hari',
                    'value'=>function($data){
                        return $data->hari;
                    }
		),
                 array(
                    'name'=> 'jam',
                    'value'=>function($data){
                        return $data->jammulai." s/d ".$data->jamselesai;
                    }
		),
                 array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
              
			'template' => '{delete}',
			'buttons' => array(
				
                                'delete' => array(
					'url' => 'Yii::app()->createUrl("/absensi/deletepergantian", array("id" => $data->id_pergantian))'
				)
			)
		),
                        
	),
));
