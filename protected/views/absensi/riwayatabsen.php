<?php
/* @var $this AbsensiController */
/* @var $model Absensi */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#absensi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Riwayat Absensi</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'absensi-grid',
	'dataProvider'=>$data,
        'template'=>"{items}",
	'columns'=>array(
		'id_karyawan',
		'tanggal',
		'hari',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions' => array('width' => '3%'),
			'template' => '{view}',
			'buttons' => array(
				'view' => array(
					'url' => 'Yii::app()->createUrl("/absensi/viewriwayat", array("id" => $data->id))'
				),
			)
		),
		
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah',
			'url'=>array('create'),
		)); ?>
   </td>
  </tr>
</table>
