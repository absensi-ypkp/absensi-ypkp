<?php
/* @var $this AbsensiController */
/* @var $model Absensi */
?>

<h1>Lihat Data Absensi #<?php echo $model->idKaryawan->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name'=> 'Nip',
                    'value'=>function($data){
                        return $data->id_karyawan;
                    }
		),
                array(
                    'name'=> 'Nama',
                    'value'=>function($data){
                        return $data->idKaryawan->nama;
                    }
		),
		'tanggal',
		'hari',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'id'=>'grid_pergantian',
	'dataProvider'=>$data,
        'template'=>"{items}",
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
       
		 array(
                    'name'=> 'Mata Kuliah',
                    'value'=>function($data){
                        return $data->idMtk->nama;
                    }
		),
                 array(
                    'name'=> 'Ruangan',
                    'value'=>function($data){
                        return $data->idRuangan->kode;
                    }
		),
                array(
                    'name'=> 'Kelas',
                    'value'=>function($data){
                        return $data->kodeKelas->nama_kelas;
                    }
		),
                array(
                    'name'=> 'Hari',
                    'value'=>function($data){
                        return $data->hari;
                    }
		),
                 array(
                    'name'=> 'jam',
                    'value'=>function($data){
                        return $data->jammulai." s/d ".$data->jamselesai;
                    }
		),
                        
                array(
			'class'=>'CButtonColumn',
			'template' => '{tambah}',
			'buttons' => array(
				'tambah' => array(
                                        'label'=>'Pergantian',
					'url' => 'Yii::app()->createUrl("/absensi/createpergantian", array("id" => $data->kode_jadwal))'
				),
			)
		),
	),
));
