<?php
/* @var $this JenisController */
/* @var $model Jenis */

$this->breadcrumbs=array(
	'Jabatan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Jenis', 'url'=>array('index')),
	array('label'=>'Manage Jenis', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Jabatan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>