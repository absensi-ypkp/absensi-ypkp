<?php
/* @var $this JenisController */
/* @var $model Jenis */

$this->breadcrumbs=array(
	'Jabatan'=>array('index'),
	$model->jenis,
);

$this->menu=array(
	array('label'=>'List Jenis', 'url'=>array('index')),
	array('label'=>'Create Jenis', 'url'=>array('create')),
	array('label'=>'Update Jenis', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Jenis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Jenis', 'url'=>array('admin')),
);
?>

<h1>Lihat Data Jabatan #<?php echo $model->jenis; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jenis',
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Kembali',
			'url'=>array('index'),
		)); ?>
   </td>
  </tr>
</table>
