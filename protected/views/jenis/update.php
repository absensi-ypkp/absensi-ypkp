<?php
/* @var $this JenisController */
/* @var $model Jenis */

$this->breadcrumbs=array(
	'Jabatan'=>array('index'),
	$model->jenis=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'List Jenis', 'url'=>array('index')),
	array('label'=>'Create Jenis', 'url'=>array('create')),
	array('label'=>'View Jenis', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Jenis', 'url'=>array('admin')),
);
?>

<h1>Ubah Data Jabatan #<?php echo $model->jenis; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>