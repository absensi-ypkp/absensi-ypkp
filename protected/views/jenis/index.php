<?php
/* @var $this JenisController */
/* @var $model Jenis */

$this->breadcrumbs=array(
	'Jabatan'=>array('index'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'List Jenis', 'url'=>array('index')),
	array('label'=>'Create Jenis', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jenis-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Data Jabatan</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'jenis-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{items}",
	'columns'=>array(
		'id',
		'jenis',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
		),
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah Data',
			'url'=>array('create'),
		)); ?>
   </td>
  </tr>
</table>
