<?php
/* @var $this JenisController */
/* @var $model Jenis */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
)); ?>

	<div class="row">
		<?php echo $form->textFieldRow($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->textFieldRow($model,'jenis',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Cari',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->