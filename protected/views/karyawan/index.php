<?php
/* @var $this KaryawanController */
/* @var $model Karyawan */

$this->breadcrumbs=array(
	'Karyawan'=>array('index'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'List Karyawan', 'url'=>array('index')),
	array('label'=>'Create Karyawan', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#karyawan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Data Karyawan</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
        'id'=>'karyawan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{items}",
	'columns'=>array(
                 array(
                    'header' => 'No',
                    'value' => '$row+1',
                    'htmlOptions' => array('width' => '3%')
                    ),
		'nip',
		'nama',
		array(
		'name'=> 'jenis_id',
                        'filter'=>CHtml::activeDropDownList($model, 'jenis_id', CHtml::listData(
				Jenis::model()->findAll(), "id", "jenis"),
				array(
					'empty'=> '=== Pilih Jabatan ==='
					)
				),
			'value'=>function($data){
				return $data->jenis->jenis;
			}
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        
                        'htmlOptions'=>array('style'=>'width: 50px'),
		),
	),
)); ?>

<table width="100%" border="0">
  <tr>
    <td align="right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'type'=>'primary',
			'label'=>'Tambah Data',
			'url'=>array('create'),
		)); ?>
   </td>
  </tr>
</table>
