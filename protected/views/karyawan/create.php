<?php
/* @var $this KaryawanController */
/* @var $model Karyawan */

$this->breadcrumbs=array(
	'Karyawan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Karyawan', 'url'=>array('index')),
	array('label'=>'Manage Karyawan', 'url'=>array('admin')),
);
?>

<h1>Tambah Data Karyawan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>