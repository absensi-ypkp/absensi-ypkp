<?php

/**
 * This is the model class for table "pergantian".
 *
 * The followings are the available columns in table 'pergantian':
 * @property integer $id_pergantian
 * @property integer $id_absensi
 * @property integer $kode_jadwal
 * @property integer $id_ruangan
 * @property string $tanggal
 * @property string $hari
 * @property string $jammulai
 * @property string $jamselesai
 *
 * The followings are the available model relations:
 * @property Absensi $idAbsensi
 * @property Jadwal $kodeJadwal
 * @property Ruangan $idRuangan
 */
class Pergantian extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pergantian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_absensi, kode_jadwal, id_ruangan, tanggal, hari, jammulai, jamselesai', 'required'),
			array('id_absensi, kode_jadwal, id_ruangan', 'numerical', 'integerOnly'=>true),
			array('hari', 'length', 'max'=>10),
			array('jammulai, jamselesai', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pergantian, id_absensi, kode_jadwal, id_ruangan, tanggal, hari, jammulai, jamselesai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAbsensi' => array(self::BELONGS_TO, 'Absensi', 'id_absensi'),
			'kodeJadwal' => array(self::BELONGS_TO, 'Jadwal', 'kode_jadwal'),
			'idRuangan' => array(self::BELONGS_TO, 'Ruangan', 'id_ruangan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pergantian' => 'Id Pergantian',
			'id_absensi' => 'Id Absensi',
			'kode_jadwal' => 'Kode Jadwal',
			'id_ruangan' => 'Ruangan',
			'tanggal' => 'Tanggal',
			'hari' => 'Hari',
			'jammulai' => 'Jam Mulai',
			'jamselesai' => 'Jam Selesai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pergantian',$this->id_pergantian);
		$criteria->compare('id_absensi',$this->id_absensi);
		$criteria->compare('kode_jadwal',$this->kode_jadwal);
		$criteria->compare('id_ruangan',$this->id_ruangan);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('hari',$this->hari,true);
		$criteria->compare('jammulai',$this->jammulai,true);
		$criteria->compare('jamselesai',$this->jamselesai,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pergantian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
