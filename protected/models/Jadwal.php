<?php

/**
 * This is the model class for table "jadwal".
 *
 * The followings are the available columns in table 'jadwal':
 * @property integer $kode_jadwal
 * @property string $hari
 * @property string $id_karyawan
 * @property integer $id_mtk
 * @property integer $kode_kelas
 * @property integer $id_ruangan
 * @property string $jammulai
 * @property string $jamselesai
 *
 * The followings are the available model relations:
 * @property Matakuliah $idMtk
 * @property Kelas $kodeKelas
 * @property Ruangan $idRuangan
 * @property Karyawan $idKaryawan
 * @property Pergantian[] $pergantians
 */
class Jadwal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jadwal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hari, id_karyawan, id_mtk, kode_kelas, id_ruangan, jammulai, jamselesai', 'required'),
			array('id_mtk, kode_kelas, id_ruangan', 'numerical', 'integerOnly'=>true),
			array('hari', 'length', 'max'=>10),
			array('id_karyawan', 'length', 'max'=>20),
			array('jammulai, jamselesai', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kode_jadwal, hari, id_karyawan, id_mtk, kode_kelas, id_ruangan, jammulai, jamselesai, jam', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMtk' => array(self::BELONGS_TO, 'Matakuliah', 'id_mtk'),
			'kodeKelas' => array(self::BELONGS_TO, 'Kelas', 'kode_kelas'),
			'idRuangan' => array(self::BELONGS_TO, 'Ruangan', 'id_ruangan'),
			'idKaryawan' => array(self::BELONGS_TO, 'Karyawan', 'id_karyawan'),
			'pergantians' => array(self::HAS_MANY, 'Pergantian', 'kode_jadwal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kode_jadwal' => 'Kode Jadwal',
			'hari' => 'Hari',
			'id_karyawan' => 'Dosen',
			'id_mtk' => 'Mata Kuliah',
			'kode_kelas' => 'Kelas',
			'id_ruangan' => 'Ruangan',
			'jammulai' => 'Jam Mulai',
			'jamselesai' => 'Jam Selesai',
                        'jam' => 'Jam',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kode_jadwal',$this->kode_jadwal);
		$criteria->compare('hari',$this->hari,true);
		$criteria->compare('id_karyawan',$this->id_karyawan,true);
		$criteria->compare('id_mtk',$this->id_mtk);
		$criteria->compare('kode_kelas',$this->kode_kelas);
		$criteria->compare('id_ruangan',$this->id_ruangan);
		$criteria->compare('jammulai',$this->jammulai,true);
		$criteria->compare('jamselesai',$this->jamselesai,true);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jadwal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function hari()
	{
		return array(
			'Senin'=>'Senin',
			'Selasa'=>'Selasa',
			'Rabu'=>'Rabu',
			'Kamis'=>'Kamis',
			"Jum'at"=>"Jum'at",
			'Sabtu'=>'Sabtu',
		);
	}
}
